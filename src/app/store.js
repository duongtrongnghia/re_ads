import { configureStore } from '@reduxjs/toolkit';
import homeReducer from '../pages/users_manage/homeSlice';
import loginReducer from '../pages/login/loginSlice';

export default configureStore({
  reducer: {
    home: homeReducer,
    login: loginReducer,
  },
});
