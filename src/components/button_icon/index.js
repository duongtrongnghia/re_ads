import React from 'react';
import { Link } from 'react-router-dom';

function ButtonIcon({ icon, onClick, className, label }) {
  const onClickButton = () => {
    if (typeof onClick === 'function') {
      onClick();
    }
  };

  let classes = [];
  className && classes.push(className);

  return (
    <Link to='#' className={classes.join(' ')} onClick={onClickButton}>
      {icon && icon}
      {label && <span>{label}</span>}
    </Link>
  );
}

export default ButtonIcon;
