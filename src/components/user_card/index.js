import React from 'react';

function UserCard({ name, email, iconRight }) {
  return (
    <div className='user-card'>
      <div className='user-avatar'>
        <span>AB</span>
      </div>
      <div className='user-info'>
        <span className='lead-text'>{name}</span>
        <span className='sub-text'>{email}</span>
      </div>
      <div className='user-action'>{iconRight && iconRight}</div>
    </div>
  );
}

export default UserCard;
