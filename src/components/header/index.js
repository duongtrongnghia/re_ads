import React from 'react';
import ButtonIcon from '../button_icon';
import ModalDropdown from '../modal_dropdown';
import HeaderNotificationDropdown from './header_notification_dropdown';
import HeaderProfileDropdown from './header_profile_dropdown';

function Header() {
  return (
    <div className='nk-header nk-header-fluid nk-header-fixed is-light'>
      <div className='container-fluid'>
        <div className='nk-header-wrap'>
          <div className='nk-menu-trigger d-xl-none ml-n1'>
            <ButtonIcon
              className='nk-nav-toggle nk-quick-nav-icon d-xl-none'
              icon={<em className='icon ni ni-menu' />}
            />
          </div>
          <div className='nk-header-brand d-xl-none'>Logo here</div>
          <div className='nk-header-tools'>
            <ul className='nk-quick-nav'>
              <li className='dropdown user-dropdown show'>
                <ModalDropdown
                  mobileCenter={true}
                  target={
                    <div className='dropdown-toggle'>
                      <div className='user-toggle'>
                        <div className='user-avatar sm'>
                          <em className='icon ni ni-user-alt'></em>
                        </div>
                        <div className='user-info d-none d-md-block'>
                          <div className='user-status user-status-unverified'>
                            Unverified
                          </div>
                          <div className='user-name dropdown-indicator'>
                            Abu Bin Ishityak
                          </div>
                        </div>
                      </div>
                    </div>
                  }>
                  <HeaderProfileDropdown />
                </ModalDropdown>
              </li>
              <li className='dropdown notification-dropdown mr-n1'>
                <ModalDropdown
                  mobileCenter={true}
                  target={
                    <ButtonIcon
                      className='dropdown-toggle nk-quick-nav-icon'
                      icon={
                        <div className='icon-status icon-status-info'>
                          <em className='icon ni ni-bell'></em>
                        </div>
                      }
                    />
                  }>
                  <HeaderNotificationDropdown />
                </ModalDropdown>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Header;
