import React from 'react';

function HeaderProfileDropdown(props) {
  return (
    <div className='dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1 show position-relative'>
      <div className='dropdown-inner user-card-wrap bg-lighter d-md-block'>
        <div className='user-card'>
          <div className='user-avatar'>
            <span>AB</span>
          </div>
          <div className='user-info'>
            <span className='lead-text'>Abu Bin Ishtiyak</span>
            <span className='sub-text'>info@softnio.com</span>
          </div>
        </div>
      </div>
      <div className='dropdown-inner'>
        <ul className='link-list'>
          <li>
            <a href='/demo5/user-profile-regular.html'>
              <em className='icon ni ni-user-alt'></em>
              <span>Thông tin tài khoản</span>
            </a>
          </li>
          <li>
            <a href='/demo5/user-profile-setting.html'>
              <em className='icon ni ni-setting-alt'></em>
              <span>Cài đặt tài khoản</span>
            </a>
          </li>
        </ul>
      </div>
      <div className='dropdown-inner'>
        <ul className='link-list'>
          <li>
            <a href='#'>
              <em className='icon ni ni-signout'></em>
              <span>Đăng xuất</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default HeaderProfileDropdown;
