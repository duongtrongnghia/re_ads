import React from 'react';

function PaginationItem({ label, active, onClick }) {
  let classes = ['page-link'];

  const onClickItem = () => {
    if (typeof onClick === 'function') {
      onClick(label);
    }
  };

  if (active) {
    classes.push('active');
  }
  return (
    <li className='page-item' onClick={onClickItem}>
      <div className={classes.join(' ')}>{label}</div>
    </li>
  );
}

export default PaginationItem;
