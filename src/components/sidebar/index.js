import React from 'react';
import ButtonIcon from '../button_icon';
import SidebarListMenu from './sidebar_list_menu';
import SidebarProfile from './sidebar_profile';

function SideBar() {
  return (
    <div
      className='nk-sidebar nk-sidebar-fixed nk-sidebar-mobile'
      data-content='sidebarMenu'>
      <div className='nk-sidebar-element nk-sidebar-head'>
        <div className='nk-sidebar-brand'>Logo here</div>
        <div className='nk-menu-trigger mr-n2'>
          <ButtonIcon icon={<em className='icon ni ni-arrow-left' />} />
        </div>
      </div>
      <div className='nk-sidebar-element'>
        <div className='nk-sidebar-body' data-simplebar>
          <div className='nk-sidebar-content overflow-auto'>
            <SidebarProfile />
            <SidebarListMenu />
          </div>
        </div>
      </div>
    </div>
  );
}

export default SideBar;
