import React, { useState } from 'react';
import UserCard from '../../user_card';
import MenuProfileItem from '../menu_profile_item';

function SidebarProfile() {
  const [isExpand, setIsExpand] = useState(false);
  const onClickToggleExpand = () => {
    setIsExpand((prevIsExpand) => !prevIsExpand);
  };

  const expandClasses = ['nk-profile-content toggle-expand-content'];
  if (isExpand) {
    expandClasses.push('d-block');
  }
  return (
    <div className='nk-sidebar-widget nk-sidebar-widget-full d-xl-none pt-0'>
      <div
        className='nk-profile-toggle toggle-expand'
        onClick={onClickToggleExpand}>
        <div className='user-card-wrap'>
          <UserCard
            name='Dương Trọng Nghĩa'
            email='trongnghia.it.ht@gmail.com'
            iconRight={<em className='icon ni ni-chevron-down' />}
          />
        </div>
      </div>
      <div className={expandClasses.join(' ')} data-content='sidebarProfile'>
        <ul className='link-list'>
          <MenuProfileItem
            icon={<em className='icon ni ni-user-alt' />}
            title='Thông tin tài khoản'
          />
          <MenuProfileItem
            icon={<em className='icon ni ni-setting-alt' />}
            title='Cài đặt tài khoản'
          />
        </ul>
        <ul className='link-list'>
          <MenuProfileItem
            icon={<em className='icon ni ni-signout' />}
            title='Đăng xuất'
          />
        </ul>
      </div>
    </div>
  );
}

export default SidebarProfile;
