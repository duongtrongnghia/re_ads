import React from 'react';

function MenuItem({ title, icon }) {
  return (
    <li className='nk-menu-item'>
      <a href='html/crypto/index.html' className='nk-menu-link'>
        <span className='nk-menu-icon'>{icon && icon}</span>
        <span className='nk-menu-text'>{title}</span>
      </a>
    </li>
  );
}

export default MenuItem;
