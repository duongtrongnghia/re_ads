import React from 'react';
import MenuHeader from '../menu_header';
import MenuItem from '../menu_item';

function SidebarListMenu(props) {
  return (
    <div className='nk-sidebar-menu'>
      <ul className='nk-menu'>
        <MenuHeader title='Menu' />
        <MenuItem
          icon={<em className='icon ni ni-dashboard' />}
          title='Dashboard'
        />
        <MenuHeader title='Quản lý người dùng' />
        <MenuItem
          icon={<em className='icon ni ni-users' />}
          title='Danh sách người dùng'
        />
        <MenuHeader title='Quản lý bài viết' />
        <MenuItem
          icon={<em className='icon ni ni-file-docs' />}
          title='Danh sách bài viết'
        />
        <MenuItem
          icon={<em className='icon ni ni-edit-alt' />}
          title='Bài viết mới'
        />
        <MenuItem
          icon={<em className='icon ni ni-edit-alt' />}
          title='Bài viết cần phê duyệt'
        />
        <MenuHeader title='Quản lý danh mục' />
        <MenuItem
          icon={<em className='icon ni ni-file-docs' />}
          title='Danh sách danh mục'
        />
      </ul>
    </div>
  );
}

export default SidebarListMenu;
