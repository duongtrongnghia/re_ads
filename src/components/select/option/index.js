import React from 'react';

function Option({ children, value, onClick, active }) {
  const onClickOption = () => {
    if (typeof onClick === 'function') {
      onClick(value, children);
    }
  };

  const classes = ['select2-results__option select-option'];
  if (active) {
    classes.push('select2-results__option--highlighted');
  }
  return (
    <li
      onClick={onClickOption}
      class={classes.join(' ')}
      role='option'
      aria-selected='false'
      data-select2-id='select2-jqei-result-i3tf-group'>
      {children && children}
    </li>
  );
}

export default Option;
