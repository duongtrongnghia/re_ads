import { createSlice } from "@reduxjs/toolkit";

export const homeSlice = createSlice({
    name: 'home',
    initialState: {

    },
    reducers: {

    }
});

export const { } = homeSlice.actions;

export const selectHomeState = state => state.home;

export default homeSlice.reducer;