import React, { useState } from 'react';
import UserManageDetail from './user_manage_detail';
import UsersManageList from './user_manage_list';

import { usePrevious } from '../../custom_hook/usePrevious';

const PAGE_USER_MANAGE_LIST = 'page_user_manage_list';
const PAGE_USER_MANAGE_DETAIL = 'page_user_manage_detail';

function UserManage(props) {
  const [currentPage, setCurrentPage] = useState(PAGE_USER_MANAGE_LIST);
  let prevPage = usePrevious(currentPage);

  const listPage = [
    {
      name: PAGE_USER_MANAGE_LIST,
      component: UsersManageList,
    },
    {
      name: PAGE_USER_MANAGE_DETAIL,
      component: UserManageDetail,
    },
  ];

  const onChangePage = (page) => {
    setCurrentPage(page);
  };

  const onBackToPrevPage = () => {
    onChangePage(prevPage);
  };

  const renderPage = (props) => {
    let Page = <div></div>;
    for (const page of listPage) {
      if (page.name === currentPage) {
        Page = page.component;
      }
    }
    return (
      <Page
        {...props}
        onChangePage={onChangePage}
        onBackToPrevPage={onBackToPrevPage}
      />
    );
  };

  return <React.Fragment>{renderPage(props)}</React.Fragment>;
}

export default UserManage;
