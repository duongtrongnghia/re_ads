import React from 'react';

function UserManageDetail(props) {
  return (
    <div className='nk-content '>
      <div className='container-fluid'>
        <div className='nk-content-inner'>
          <div className='nk-content-body'>
            <button onClick={props.onBackToPrevPage}>Quay lại</button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default UserManageDetail;
