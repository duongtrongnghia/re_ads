import React from 'react';
import ButtonIcon from '../../../../components/button_icon';

function UsersSearch(props) {
  return (
    <div className='card-search search-wrap' data-search='search'>
      <div className='card-body'>
        <div className='search-content'>
          <ButtonIcon
            className='search-back btn btn-icon toggle-search'
            icon={<em className='icon ni ni-arrow-left'></em>}
          />
          <input
            type='text'
            className='form-control border-transparent form-focus-none'
            placeholder='Tìm kiếm theo tên hoặc email'
          />
          <button className='search-submit btn btn-icon'>
            <em className='icon ni ni-search'></em>
          </button>
        </div>
      </div>
    </div>
  );
}

export default UsersSearch;
