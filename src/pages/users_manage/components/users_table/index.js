import React from 'react';
import ButtonIcon from '../../../../components/button_icon';
import ModalDropdown from '../../../../components/modal_dropdown';
import UsersModalAction from '../users_modal_action';

function UsersTable(props) {
  return (
    <div className='nk-tb-list nk-tb-ulist'>
      <div className='nk-tb-item nk-tb-head'>
        <div className='nk-tb-col nk-tb-col-check'>
          <div className='custom-control custom-control-sm custom-checkbox notext'>
            <input type='checkbox' className='custom-control-input' id='uid' />
            <label className='custom-control-label'></label>
          </div>
        </div>
        <div className='nk-tb-col'>
          <span className='sub-text'>User</span>
        </div>
        <div className='nk-tb-col tb-col-mb'>
          <span className='sub-text'>Balance</span>
        </div>
        <div className='nk-tb-col tb-col-md'>
          <span className='sub-text'>Phone</span>
        </div>
        <div className='nk-tb-col tb-col-lg'>
          <span className='sub-text'>Verified</span>
        </div>
        <div className='nk-tb-col tb-col-lg'>
          <span className='sub-text'>Last Login</span>
        </div>
        <div className='nk-tb-col tb-col-md'>
          <span className='sub-text'>Status</span>
        </div>
        <div className='nk-tb-col nk-tb-col-tools text-right'></div>
      </div>

      <div className='nk-tb-item'>
        <div className='nk-tb-col nk-tb-col-check'>
          <div className='custom-control custom-control-sm custom-checkbox notext'>
            <input type='checkbox' className='custom-control-input' id='uid1' />
            <label className='custom-control-label'></label>
          </div>
        </div>
        <div className='nk-tb-col'>
          <a href='html/general/user-details-regular.html'>
            <div className='user-card'>
              <div className='user-avatar bg-primary'>
                <span>AB</span>
              </div>
              <div className='user-info'>
                <span className='tb-lead'>
                  Abu Bin Ishtiyak{' '}
                  <span className='dot dot-success d-md-none ml-1'></span>
                </span>
                <span>info@softnio.com</span>
              </div>
            </div>
          </a>
        </div>
        <div className='nk-tb-col tb-col-mb'>
          <span className='tb-amount'>
            35,040.34 <span className='currency'>USD</span>
          </span>
        </div>
        <div className='nk-tb-col tb-col-md'>
          <span>+811 847-4958</span>
        </div>
        <div className='nk-tb-col tb-col-lg'>
          <ul className='list-status'>
            <li>
              <em className='icon text-success ni ni-check-circle'></em>{' '}
              <span>Email</span>
            </li>
            <li>
              <em className='icon ni ni-alert-circle'></em> <span>KYC</span>
            </li>
          </ul>
        </div>
        <div className='nk-tb-col tb-col-lg'>
          <span>10 Feb 2020</span>
        </div>
        <div className='nk-tb-col tb-col-md'>
          <span className='tb-status text-success'>Active</span>
        </div>
        <div className='nk-tb-col nk-tb-col-tools'>
          <ul className='nk-tb-actions gx-1'>
            <li>
              <div className='drodown'>
                <ModalDropdown
                  target={
                    <ButtonIcon
                      className='dropdown-toggle btn btn-icon btn-trigger'
                      icon={<em className='icon ni ni-more-h'></em>}
                    />
                  }>
                  <UsersModalAction />
                </ModalDropdown>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}

export default UsersTable;
