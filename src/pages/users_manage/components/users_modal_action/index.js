import React from 'react';
import ButtonIcon from '../../../../components/button_icon';

function UsersModalAction() {
  return (
    <div
      className='dropdown-menu dropdown-menu-right show position-relative'
      x-placement='bottom-end'>
      <ul className='link-list-opt no-bdr'>
        <li>
          <ButtonIcon
            icon={<em className='icon ni ni-eye'></em>}
            label='Xem chi tiết'
          />
        </li>
        {/* <li className='divider'></li> */}
        <li>
          <ButtonIcon
            icon={<em className='icon ni ni-repeat'></em>}
            label='Xóa tài khoản'
          />
        </li>
        <li>
          <ButtonIcon
            icon={<em className='icon ni ni-na'></em>}
            label='Khóa tài khoản'
          />
        </li>
      </ul>
    </div>
  );
}

export default UsersModalAction;
