import React from 'react';
import Header from '../../components/header';
import SideBar from '../../components/sidebar';

function MainSite({ component }) {
  return (
    <div className='nk-body npc-crypto has-sidebar has-sidebar-fat ui-clean'>
      <div className='nk-app-root'>
        <div className='nk-main'>
          <SideBar />
          <div className='nk-wrap'>
            <Header />
            {component}
          </div>
        </div>
      </div>
    </div>
  );
}

export default MainSite;
