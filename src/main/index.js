import React, { lazy } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import MainSite from './main_site';

import routes from '../pages/routes';
import Loading from '../components/loading';

const LoginPage = lazy(() => import('../pages/login'));

function App() {
  return (
    <Router>
      <Switch>
        {routes.map(({ component: Component, path, ...rest }) => {
          return (
            <Route
              render={(props) => (
                <React.Suspense fallback={<Loading />}>
                  <MainSite component={<Component {...props} />} />
                </React.Suspense>
              )}
              key={path}
              path={path}
              {...rest}
            />
          );
        })}
        <React.Suspense fallback={<Loading />}>
          <Route exact path='/login' component={() => <LoginPage />} />
        </React.Suspense>
      </Switch>
    </Router>
  );
}

export default App;
